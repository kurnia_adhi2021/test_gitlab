<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\User;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    public function index()
    {
        $students = Student::orderBy('id','DESC')->get();
        return view('students.index', compact('students'));
    }

    public function create()
    {
        return view('students.create');
    }

    public function store(Request $request)
    {
        $students = Student::create([
            'first_name'=> $request->first_name,
            'last_name'=> $request->last_name,
            'address'=> $request->address,
            'phone'=> $request->phone,
        ]);
        return redirect('/student');
    }

    public function edit($id)
    {
        $student = Student::find($id);
        return view ('students.edit',compact('student'));
    }

    public function update(Request $request,$id)
    {
        $student = Student::find($id);
        $student->update([
            'first_name'=> $request->first_name,
            'last_name'=> $request->last_name,
            'address'=> $request->address,
            'phone'=> $request->phone,
        ]);

        return redirect('/student');
    }

    public function destroy($id)
    {
        $student = Student::find($id);
        $student->delete();

        return redirect('/student');
    }

}
