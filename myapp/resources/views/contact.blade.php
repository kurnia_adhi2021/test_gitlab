@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row my-3">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <form action="/contactSubmit" method="POST">
                        @csrf
                        <div class="form-group">
                          <label for="">Name</label>
                          <input type="text"
                            class="form-control" name="name" id="" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="">Your Mail</label>
                          <input type="email"
                            class="form-control" name="email" id="" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="">Message :</label>
                            <textarea class="form-control" name="message" id="" rows="3"></textarea>
                          </div>
                        <button type="submit" class="btn btn-primary btn-block">submit</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="embed-responsive embed-responsive-4by3">
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.738335219999!2d107.60482541384775!3d-6.921851769670767!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e89dbe0ec231%3A0x177412aac90cd065!2sAlun-Alun%20Kota%20Bandung!5e0!3m2!1sen!2sid!4v1648536823126!5m2!1sen!2sid"
                            width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""
                            aria-hidden="false" tabindex="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    </div>
</div>
@endsection