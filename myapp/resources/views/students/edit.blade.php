@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row mt-3">
            <div class="col-md-8">
                <div class="card my-3">
                    <div class="card-body">
                        <form action="/student/{{$student->id}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="">Nama Depan</label>
                                <input type="text" class="form-control" name="first_name" value="{{$student->first_name}}">
                              </div>
                              <div class="form-group">
                                <label for="">Nama Belakang</label>
                                <input type="text" class="form-control" name="last_name" value="{{$student->last_name}}">
                              </div>
                              <div class="form-group">
                                <label for="">Alamat</label>
                                <input type="text" class="form-control" name="address" value="{{$student->address}}">
                              </div>
                              <div class="form-group">
                                <label for="">Phone</label>
                                <input type="text" class="form-control" name="phone" value="{{$student->phone}}">
                              </div>
                            <button type="submit" class="btn btn-primary btn-sm">Update</button>
                          </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection