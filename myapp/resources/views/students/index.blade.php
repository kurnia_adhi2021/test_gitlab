@extends('layouts.master')

@section('content')    

<div class="container">
  @if (Auth::user())
      @if(Auth::user()->role == 1)
      <a href="/student/create" class="btn btn-primary btn-sm mt-2">
        create data
      </a>
    <div class="row mt-3">
        <table class="table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Depan</th>
                    <th>Nama Belakang</th>
                    <th>Alamat</th>
                    <th>Telephone</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                    @php
                        $no=1;
                    @endphp
                @forelse ($students as $student)    
                <tr>
                    <td>{{$no++}}</td>
                    <td>{{$student->first_name}}</td>
                    <td>{{$student->last_name}}</td>
                    <td>{{$student->address}}</td>
                    <td>{{$student->phone}}</td>
                    <td>
                        <form action="/student/{{$student->id}}" method="post">
                            @csrf
                            @method('DELETE')
                            <a href="/student/{{$student->id}}/edit" class="btn btn-warning btn-sm">edit</a>
                            <input type="submit" class="btn btn-danger btn-sm" value="delete">
                        </form>
                    </td>
                </tr>
                @empty
                    
                @endforelse
            </tbody>
        </table>
    </div>
      @endif
  @endif
  @if (Auth::user())
      @if(Auth::user()->role == 0)
      <div class="row my-5">
        @forelse ($students as $student)    
        <div class="col-md-3 mt-2">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">{{$student->first_name}}</h4>
                <h5 class="card-title">{{$student->last_name}}</h5>
                <p class="card-text">{{$student->address}}</p>
                <p class="card-text">{{$student->phone}}</p>
              </div>
            </div>
        </div>
        @empty
            <p>no data</p>
        @endforelse
      </div>
      @endif
  @endif
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
          {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
        </div>
      </div>
    </div>
  </div>

@endsection
