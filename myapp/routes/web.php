<?php



// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('about');
});

Route::get('/contact','ContactController@index');
Route::post('/contactSubmit','ContactController@contactSubmit');

Route::group(['middleware' => ['auth','cekRole:1']],function(){ 
    Route::get('/student/create','StudentController@create');
    Route::post('/student','StudentController@store');
    Route::get('/student/{id}/edit','StudentController@edit');
    Route::put('/student/{id}','StudentController@update');
    Route::delete('/student/{id}','StudentController@destroy');
});

Route::group(['middleware' => ['auth','cekRole:1,0']],function(){
    Route::get('/student','StudentController@index');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
